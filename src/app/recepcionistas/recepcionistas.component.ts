import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Recepcionista } from './recepcionista';
import { RecepcionistaService } from './recepcionista.service'
import swal from 'sweetalert2'
import { ModalService } from '../clientes/detalle/modal.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../usuarios/auth.service';

@Component({
  selector: 'app-recepcionistas',
  templateUrl: './recepcionistas.component.html',
  styleUrls: ['./recepcionistas.component.css']
})
export class RecepcionistasComponent implements OnInit {

  recepcionistas: Recepcionista[] = [];

  paginador: any;

  enlacePaginador: string = 'recepcionistas/page';

  recepcionistaSeleccionado: Recepcionista;

  filterpost = '';

  constructor(private recepcionistaService: RecepcionistaService,
    private modalService: ModalService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService) { }


  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.recepcionistaService.getAllRecepcionistas().subscribe(
        response => {
          this.recepcionistas = response as Recepcionista[];

        });
    });
    this.modalService.notificarUpload.subscribe(recepcionista => {
      this.recepcionistas = this.recepcionistas.map(recepcionistaOriginal => {
        if (recepcionista.id == recepcionistaOriginal.id) {
          recepcionistaOriginal.foto = recepcionista.foto;
        }
        return recepcionistaOriginal;
      })
    })
  }

  delete(recepcionista: Recepcionista): void {
    swal.fire({
      title: `Está seguro de querer eliminar a ${recepcionista.nombre} ${recepcionista.apellidoPaterno} ? `,
      text: "Esta accion no puede ser revertida",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
    }).then((result) => {
      if (result.value) {

        this.recepcionistaService.delete(recepcionista.id).subscribe(
          () => {
            this.recepcionistas = this.recepcionistas.filter(rec => rec !== recepcionista)
            swal.fire(
              'Recepcionista Eliminado!',
              `${recepcionista.nombre} ${recepcionista.apellidoPaterno}  eliminado con éxito.`,
              'success'
            )
          }
        )
      }
    });
  }

  abrirModal(recepcionista: Recepcionista) {
    this.recepcionistaSeleccionado = recepcionista;
    this.modalService.abrirModal();
  }

}



