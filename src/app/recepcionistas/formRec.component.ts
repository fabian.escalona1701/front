import { Component, OnInit } from '@angular/core';
import { Recepcionista } from './recepcionista';
import { RecepcionistaService } from './recepcionista.service'
import { Router, ActivatedRoute } from '@angular/router'
import swal from 'sweetalert2'
import { Usuario } from '../usuarios/usuario';
import { UsuarioService } from '../usuarios/usuario.service';

@Component({
  selector: 'app-formRec',
  templateUrl: './formRec.component.html'
})
export class FormRecComponent implements OnInit {

  public recepcionista: Recepcionista = new Recepcionista()
  public titulo: string = "REGISTRO DE RECEPCIONISTA"
  public usuario: Usuario = new Usuario()
  public errores: string[];



  constructor(private recepcionistaService: RecepcionistaService,
    private usuarioService: UsuarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarRecepcionista()
  }



  public cargarRecepcionista(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.recepcionistaService.getRecepcionista(id).subscribe((rec) => {
          this.recepcionista = rec
          this.usuarioService.getUsuarioByUsername(this.recepcionista.correo).subscribe(
            user => this.usuario = user)
        })
      }
    })
  }


  public create(): void {

    this.recepcionistaService.create(this.recepcionista)
      .subscribe(
        rec => {
          this.cargarUsuario();
          this.usuario.id2 = rec.id
          this.usuarioService.createUsuario(this.usuario, 3).subscribe(
            usuario => err => {
              this.errores = err.error.errors as string[];
            }
          );
          this.router.navigate(['/recepcionistas'])
          swal.fire('Nuevo recepcionista', `Recepcionista ${this.recepcionista.nombre} ${this.recepcionista.apellidoPaterno} creado con éxito!`, 'success')
        },
        err => {
          this.errores = err.error.errors as string[];
        }
      );
  }


  public update(): void {
    this.recepcionistaService.update(this.recepcionista)
      .subscribe(recepcionista => {
        this.usuario.username = this.recepcionista.correo
        this.usuario.nombre = this.recepcionista.nombre
        this.usuario.apellido = this.recepcionista.apellidoPaterno.concat(" ", this.recepcionista.apellidoMaterno)
        this.usuario.password = this.recepcionista.rut
        this.usuarioService.update(this.usuario).subscribe();
        this.router.navigate(['/recepcionistas'])
        swal.fire('Recepcionista Actualizado', `Recepcionista ${recepcionista.nombre} ${recepcionista.apellidoPaterno} actualizado con éxito!`, 'success')
      },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        });

  }

  cargarUsuario() {
    this.usuario.username = this.recepcionista.correo
    this.usuario.password = this.recepcionista.rut
    this.usuario.nombre = this.recepcionista.nombre
    this.usuario.apellido = this.recepcionista.apellidoPaterno.concat(" ", this.recepcionista.apellidoMaterno)
  }

}