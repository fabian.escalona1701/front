import { HttpEventType } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from 'src/app/clientes/detalle/modal.service';
import swal from 'sweetalert2';
import { Recepcionista } from '../recepcionista';
import { RecepcionistaService } from '../recepcionista.service';

@Component({
  selector: 'detalle-recepcionista',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleRecepcionistaComponent implements OnInit {

  @Input() recepcionista: Recepcionista;
  titulo: string = "Detalle del recepcionista";
  fotoSeleccionada: File;
  progreso: number = 0;

  constructor(private recepcionistaService: RecepcionistaService,
    public modalService: ModalService) { }

  ngOnInit(): void { }

  seleccionarFoto(event) {
    this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;

    if (this.fotoSeleccionada.type.indexOf('image') < 0) {
      swal.fire('Error seleccionando imagen: ', 'El archivo debe ser del tipo imagen', 'error')
      this.fotoSeleccionada = null;
    }
  }

  subirFoto() {

    if (!this.fotoSeleccionada) {
      swal.fire('Error Upload: ', 'Debe seleccionar una foto', 'error')
    } else {
      this.recepcionistaService.subirFoto(this.fotoSeleccionada, this.recepcionista.id)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body;
            this.recepcionista = response.recepcionista as Recepcionista;

            this.modalService.notificarUpload.emit(this.recepcionista);

            swal.fire('La imagen ha subido completamente', response.mensaje, 'success',)
          }

        });
    }
  }

  cerrarModal() {

    this.modalService.cerrarModal();
    this.fotoSeleccionada = null;
    this.progreso = 0;
  }

}
