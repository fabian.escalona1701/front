import { Injectable } from '@angular/core';
import { Recepcionista } from './recepcionista';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators'
import { AuthService } from '../usuarios/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Usuario } from '../usuarios/usuario';

@Injectable({
  providedIn: 'root'
})
export class RecepcionistaService {

  private urlEndPoint: string = 'http://localhost:8080/mfit/recepcionistas'

  private httpHeaders = new HttpHeaders({ 'content-type': 'application/json' })
  constructor(private http: HttpClient, private router: Router,
    private authService: AuthService) { }

  private agregarAuthorizationHeader() {
    let token = this.authService.token;
    if (token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e): boolean {
    if (e.status == 401) {

      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }

    if (e.status == 403) {
      swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.username} pero no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/inicio']);
      return true;
    }
    return false;
  }

  createUsuario(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>('http://localhost:8080/mfit/registro/3', usuario, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response.usuario as Usuario),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }

          if (e.status == 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            console.error(e.error.mensaje)
          }


          return throwError(e);
        })
      )
  }


  create(recepcionista: Recepcionista): Observable<Recepcionista> {
    return this.http.post<Recepcionista>(this.urlEndPoint, recepcionista, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response.recepcionista as Recepcionista),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }

          if (e.status == 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            swal.fire(e.error.mensaje, e.error.error, 'error')
          }
          return throwError(e);
        })
      )
  }

  getAllRecepcionistas(): Observable<Recepcionista[]> {

    return this.http.get(this.urlEndPoint, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as Recepcionista[])
    );
  }

  getRecepcionistas(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page, { headers: this.agregarAuthorizationHeader() }).pipe(
      map((response: any) => {
        (response.content as Recepcionista[]).map(recepcionista => {
          recepcionista.nombre = recepcionista.nombre.toUpperCase();
          recepcionista.apellidoPaterno = recepcionista.apellidoPaterno.toUpperCase();
          recepcionista.apellidoMaterno = recepcionista.apellidoMaterno.toUpperCase();

          return recepcionista;
        });
        return response;
      })
    );
  }


  getRecepcionista(id: any): Observable<Recepcionista> {
    return this.http.get<Recepcionista>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/inicio']);
        console.error(e.error.mensaje);
        swal.fire('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }
  update(recepcionista: Recepcionista): Observable<Recepcionista> {
    return this.http.put<Recepcionista>(`${this.urlEndPoint}/${recepcionista.id}`, recepcionista, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  delete(id: number): Observable<Recepcionista> {
    return this.http.delete<Recepcionista>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  subirFoto(archivo: File, id): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    let httpHeaders = new HttpHeaders();
    let token = this.authService.token;
    if (token != null) {
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    }

    const req = new HttpRequest('POST', `${this.urlEndPoint}/upload`, formData, {
      reportProgress: true,
      headers: httpHeaders
    });

    return this.http.request(req).pipe(
      catchError(e => {
        this.isNoAutorizado(e);
        return throwError(e);
      })
    );

  }


}

