import { Cliente } from "../clientes/cliente";

export class Recepcionista {
    id: number;
    rut: string;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    fechaNacimiento: string;
    correo: string;
    telefono: number;
    direccion: string;
    sueldo: number;
    foto: string;
    clientes: Array<Cliente> = [];
}
