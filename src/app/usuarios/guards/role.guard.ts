import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(public authService: AuthService,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }

    const roles = next.data['role'] as string[];
    let hasRole = false;
    roles.forEach(role => {
      if (this.authService.hasRole(role)) {
        hasRole = true;
      }
    });
    if (hasRole) { return true }

    swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.nombre} no tienes acceso a este recurso!`, 'warning');
    this.router.navigate(['/inicio']);
    return false;
  }

}
