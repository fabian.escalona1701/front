import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import swal from 'sweetalert2';
import { Observable, throwError } from 'rxjs';
import { Usuario } from './usuario';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private urlEndPoint: string = 'http://localhost:8080/mfit/usuarios'

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient, private router: Router,
    private authService: AuthService) { }

  private agregarAuthorizationHeader() {
    let token = this.authService.token;
    if (token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e): boolean {
    if (e.status == 401) {

      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }

    if (e.status == 403) {
      swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.username} pero no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/inicio']);
      return true;
    }
    return false;
  }

  getUsuarioByUsername(username: string): Observable<Usuario> {
    return this.http.get<Usuario>(`${this.urlEndPoint}/${username}`, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        catchError(e => {

          if (this.isNoAutorizado(e)) {
            return throwError(() => new Error(e));
          }
          this.router.navigate(['/inicio']);
          console.error(e.error.mensaje);
          swal.fire('Error ', 'error uwu', 'error');
          return throwError(() => new Error(e));
        })
      )
  }

  createUsuario(usuario: Usuario, role: number): Observable<Usuario> {

    return this.http.post<Usuario>(`${this.urlEndPoint}/${role}`, usuario, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response.usuario as Usuario),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }

          if (e.status == 400) {
            return throwError(e);
          }

          if (e.status == 500) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            console.error(e.error.mensaje)
          }


          return throwError(e);
        })
      )
  }

  update(usuario: Usuario): Observable<any> {
    return this.http.put<Usuario>(`${this.urlEndPoint}/${usuario.id}`, usuario, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }

        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }
}
