import { Component, OnInit } from '@angular/core';
import { Pago } from './pago';
import { PagoService } from './pago.service';



@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',

})

export class PagoComponent implements OnInit {
  public titulo = 'HISTORIAL DE PAGOS'
  pago: Pago = new Pago;
  pagos: Pago[] = [];

  constructor(private pagoService: PagoService) { }

  ngOnInit(): void {
    this.pagoService.getPagos().subscribe(
      pagos => this.pagos = pagos
    );
  }



}
