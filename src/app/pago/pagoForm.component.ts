import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router'
import swal from 'sweetalert2'
import { Pago } from './pago';
import { PagoService } from './pago.service';
import { ClienteService } from '../clientes/cliente.service';
import { Cliente } from '../clientes/cliente';
import * as moment from 'moment';
import { convertCompilerOptionsFromJson } from 'typescript';


@Component({
  selector: 'pago-form',
  templateUrl: './pagoForm.component.html'
})
export class pagoFormComponent implements OnInit {
  public titulo = 'REGISTRO DE PAGO'
  public tipoPlan: number;
  public pago: Pago = new Pago();
  public cliente: Cliente;
  public ultimopago: Pago = new Pago();
  public fechaUltimoPago: string;

  public hoy: string;

  public valorMensual = 25000;
  public valorSemestral = 100000;
  public valorAnual = 150000;



  constructor(private pagoService: PagoService, private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,

  ) { }

  ngOnInit() {
    this.pago.fechaPago = new Date().toISOString().slice(0, 10);
    this.cargarPago()

  }
  public cargarPago(): void {

    this.activatedRoute.paramMap.subscribe(params => {
      let clienteId = +params.get('idCliente');
      this.clienteService.getCliente(clienteId).subscribe(cliente => {
        this.pago.cliente = cliente
        this.pagoService.getUltimoPago(clienteId).subscribe(pago => {
          this.ultimopago = pago;
        });
      });
    });
  }



  public saluda(tipo: number, fecha: string): void {
    this.tipoPlan = tipo;
    if (this.tipoPlan == 0) {
      this.pago.valor = this.valorMensual;
    } else if (this.tipoPlan == 1) {
      this.pago.valor = this.valorSemestral;
    } else if (this.tipoPlan == 2) {
      this.pago.valor = this.valorAnual;
    }

    this.fechaUltimoPago = fecha;

    let fechaVenc: Date = new Date(this.fechaUltimoPago)
    fechaVenc.setUTCHours(24)

    this.calculaFechas()

  }


  public pagar(): void {

    moment.locale("es");
    //fechahoy
    let fechaPago = moment(this.pago.fechaPago)

    //fechaVencimiento
    let fechaV: moment.Moment;
    let aux: string
    let fechaVenc: Date

    let dia: string
    let mes: string
    let año: string
    switch (`${this.pago.plan
    }`) {
      case '0':
        aux = 'MENSUAL'
        break;
      case '1':
        aux = 'SEMESTRAL'
        break;
      case '2':
        aux = 'ANUAL'
        break;
    }

    /* let fechaTermino:Date= new Date(this.pago.fechaVencimiento)
     let fechaPago:Date=new Date(this.pago.fechaPago)
     this.pago.fechaVencimiento=fechaTermino.toUTCString()
     this.pago.fechaPago=fechaPago.toUTCString()*/

    this.pagoService.create(this.pago)
      .subscribe(pago => {
        this.router.navigate(['/clientes'])
        swal.fire('Pago Realizado', `Cliente: ${this.pago.cliente.nombre} paga un plan ${aux} `, 'success')
      },
      );

  }

  public calculaFechas(): void {

    // hay que hacer un if nomas, si la fecha de ultimo pago es menor a la fecha de hoy entonces sumar a la fecvha seleccionada por el cliente los meses/años
    moment.locale("es");
    //fechahoy
    let fechaPago = moment(this.pago.fechaPago)

    let fechaVenc: Date = new Date(this.fechaUltimoPago)
    fechaVenc.setUTCHours(24)
    //fechaUltimoPago
    let fechaV: moment.Moment;
    fechaV = moment(this.fechaUltimoPago);


    let dia: string
    let mes: string
    let año: string

    let meses: number
    if (this.tipoPlan == 0) {
      meses = 1
    } else if (this.tipoPlan == 1) {
      meses = 6
    } else {
      meses = 12
    }
    fechaV = fechaV.add(meses, 'month') //sumo meses a la fecha ultimo pago o a la fecha pago
    fechaVenc = fechaV.toDate() //Transformo el moment a Date

    //Obtengo dia mes y año para crear el string
    dia = fechaVenc.getDate().toString()
    if (dia.length <= 1) {
      dia = '0' + dia
    }

    mes = (fechaVenc.getMonth() + 1).toString(); //Se le suma uno porque en el arreglo de meses empieza desde el mes "0"
    if (mes.length <= 1) {
      mes = '0' + mes
    }
    año = fechaVenc.getFullYear().toString()

    this.pago.fechaVencimiento = (año + '-' + mes + '-' + dia)

  }



}