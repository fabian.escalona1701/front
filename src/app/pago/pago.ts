import { Cliente } from "../clientes/cliente"

export class Pago {
    id: number
    valor: number
    plan: number
    fechaVencimiento: string
    fechaPago: string
    cliente: Cliente;
}