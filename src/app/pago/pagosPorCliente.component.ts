import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pago } from './pago';
import { PagoService } from './pago.service';
import { Cliente } from '../clientes/cliente';
import { ClienteService } from '../clientes/cliente.service';
import { AuthService } from '../usuarios/auth.service';
import swal from 'sweetalert2';



@Component({
  selector: 'app-pagosPorCliente',
  templateUrl: './pagosPorCliente.component.html',

})

export class PagosPorClienteComponent implements OnInit {
  pago: Pago = new Pago;
  pagosPorCliente: Pago[] = [];
  cliente: Cliente = new Cliente()

  constructor(private pagoService: PagoService,
    private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) { }


  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      let idCliente: number = params['idCliente']
      if (this.authService.isClienteAutenticated(idCliente)) {
        this.pagoService.getPagosPorCliente(idCliente).subscribe(
          pagos => this.pagosPorCliente = pagos
        );
        this.clienteService.getCliente(idCliente).subscribe(client => this.cliente = client)
      }
    })

  }



}
