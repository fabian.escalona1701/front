import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/usuarios/auth.service';
import { Usuario } from 'src/app/usuarios/usuario';
import { UsuarioService } from 'src/app/usuarios/usuario.service';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-cambio-password',
  templateUrl: './cambio-password.component.html'
})
export class CambioPasswordComponent implements OnInit {

  titulo: string = "CAMBIO DE CONTRASEÑA DEL CLIENTE: ";
  cliente: Cliente = new Cliente()
  usuario: Usuario = new Usuario()

  passwordNueva: string = "";
  passwordRepeat: string = "";

  constructor(private clienteService: ClienteService,
    public authService: AuthService,
    private usuarioService: UsuarioService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('idCliente');
      if (id) {
        if (this.authService.isClienteAutenticated(id)) {

          this.clienteService.getCliente(id).subscribe((cliente) => {
            this.cliente = cliente

            this.usuarioService.getUsuarioByUsername(this.cliente.correo).subscribe(
              user => this.usuario = user)

          })
        }
      }
    })

  }

  cambiar() {
    this.usuario.password = this.passwordRepeat;
    this.usuarioService.update(this.usuario).subscribe();
    if (this.authService.hasRole('ROLE_USER')) {

      swal.fire('Contraseña actualizada', `Cambiaste tu contraseña con éxito. Debes volver a iniciar sesión`, 'success');
      this.authService.logout();
      this.router.navigate(['/inicio']);
    } else {
      this.router.navigate(['/clientes']);
      swal.fire('Contraseña actualizada', `Se ha cambiado la contraseña del usuario ${this.usuario.username} con éxito`, 'success');
    }

  }
}


