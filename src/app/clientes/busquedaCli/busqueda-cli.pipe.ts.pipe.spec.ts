import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const clientes = [];
    for(const cliente of value){
      let nombreCompleto = cliente.nombre.concat(" ",cliente.apellidoPaterno," ",cliente.apellidoMaterno).toLowerCase();
      if(nombreCompleto.indexOf(arg.toLowerCase()) > -1 || cliente.rut.indexOf(arg) > -1){
         clientes.push(cliente);
      };
    };
    return clientes;
  }

}