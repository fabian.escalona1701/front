import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/usuarios/auth.service';
import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';
import { ModalService } from '../detalle/modal.service';
import swal from 'sweetalert2';


@Component({
  selector: 'app-busquedaCliente',
  templateUrl: './busquedaClientes.component.html',
  styleUrls: ['./busquedaCliente.css']
})

export class BusquedaClienteComponent implements OnInit {
  clientes: Cliente[] = [];
  titulo: string = 'BUSCADOR DE CLIENTES'
  clienteSeleccionado: Cliente;
  filterpost = '';

  contadorAforo:number = 0;

  constructor(private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService,
    public authService: AuthService) {

  }


  ngOnInit(): void {
    this.crearSetItem();
    this.contadorAforo=parseInt(localStorage.getItem('contador'));
    this.activatedRoute.paramMap.subscribe(params => {
      this.clienteService.getAllClientes().subscribe(
        response => {
          this.clientes = response as Cliente[];

        });
    });
  }

  delete(cliente: Cliente): void {
    swal.fire({
      title: `Está seguro de querer eliminar a ${cliente.nombre} ${cliente.apellidoPaterno} ? `,
      text: "Esta accion no puede ser revertida",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.clienteService.delete(cliente.id).subscribe(
          response => {
            this.clientes = this.clientes.filter(cli => cli !== cliente)
            swal.fire(
              'Cliente Eliminado!',
              `El cliente ${cliente.nombre} ${cliente.apellidoPaterno} ha sido eliminado satisfactoriamente`,
              'success'
            )
          }
        )
      }
    })


    this.modalService.notificarUpload.subscribe(cliente => {
      this.clientes = this.clientes.map(clienteOriginal => {
        if (cliente.id == clienteOriginal.id) {
          clienteOriginal.foto = cliente.foto;
        }
        return clienteOriginal;
      })
    })
  }



  abrirModal(cliente: Cliente) {
    this.clienteSeleccionado = cliente;
    this.modalService.abrirModal();
  }

  aumentarContador(){
    this.contadorAforo++;
    localStorage.setItem('contador', this.contadorAforo.toString());
  }

  disminuirContador(){
    if(this.contadorAforo>0){
      this.contadorAforo--;
    }
    localStorage.setItem('contador', this.contadorAforo.toString());
    
  }

  resetearContador(){
    if(this.contadorAforo>0){
      this.contadorAforo=0;
    }
    localStorage.setItem('contador', this.contadorAforo.toString());
  }


  crearSetItem(){

    if(localStorage.getItem('contador')=='NaN'){
      console.log("DentroDecrear")
      localStorage.setItem('contador', this.contadorAforo.toString());
    }
  }

}




