import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { AuthService } from '../usuarios/auth.service';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html'
})
export class PerfilComponent implements OnInit {

  titulo = "Perfil del Cliente"
  cliente: Cliente = new Cliente();
  fotoSeleccionada: File;
  progreso: number = 0;

  constructor(private clienteService: ClienteService,
    public authService: AuthService) { }

  ngOnInit(): void {
    this.clienteService.getCliente(this.authService._usuario.id2).subscribe(cliente => {
      this.cliente = cliente;
    })
  }

  seleccionarFoto(event) {
    this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;

    if (this.fotoSeleccionada.type.indexOf('image') < 0) {
      swal.fire('Error eleccionando imagen: ', 'El archivo debe ser del tipo imagen', 'error')
      this.fotoSeleccionada = null;
    }
  }

  subirFoto() {

    if (!this.fotoSeleccionada) {
      swal.fire('Error Upload: ', 'Debe seleccionar una foto', 'error')
    } else {
      this.clienteService.subirFoto(this.fotoSeleccionada, this.cliente.id)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body;
            this.cliente = response.cliente as Cliente;

            swal.fire('La imagen ha subido completamente', response.mensaje, 'success',)
          }



        });
    }
  }

}
