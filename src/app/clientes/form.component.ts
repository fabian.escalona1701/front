import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente'
import { ClienteService } from './cliente.service'
import { Router, ActivatedRoute } from '@angular/router'
import swal from 'sweetalert2'
import { Usuario } from '../usuarios/usuario';
import { AuthService } from '../usuarios/auth.service';
import { UsuarioService } from '../usuarios/usuario.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',

})
export class FormComponent implements OnInit {

  public cliente: Cliente = new Cliente()
  public usuario: Usuario = new Usuario()
  public titulo: string = "REGISTRO DE CLIENTE"
  public errores: string[];

  constructor(private clienteService: ClienteService,
    private usuarioService: UsuarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService) { }

  ngOnInit() {
    this.cargarCliente()
  }

  public cargarCliente(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        if (this.authService.isClienteAutenticated(id)) {
          this.clienteService.getCliente(id).subscribe((cliente) => {
            this.cliente = cliente
            this.usuarioService.getUsuarioByUsername(this.cliente.correo).subscribe(
              user => this.usuario = user)
          })
        }
      }
    })
  }

  public create(): void {

    this.clienteService.create(this.cliente)
      .subscribe(
        cliente => {
          this.cargarUsuario(cliente.id);
          this.usuarioService.createUsuario(this.usuario, 1).subscribe(
            usuario => err => {
              this.errores = err.error.errors as string[];
            }
          );
          this.router.navigate(['/clientes'])
          swal.fire('Nuevo cliente', `Cliente ${cliente.nombre} creado con éxito!`, 'success')

        },
        err => {
          this.errores = err.error.errors as string[];
        }
      );
  }


  update(): void {
    this.clienteService.update(this.cliente)
      .subscribe(
        json => {
          this.usuario.username = this.cliente.correo
          this.usuario.nombre = this.cliente.nombre
          this.usuario.apellido = this.cliente.apellidoPaterno.concat(" ", this.cliente.apellidoMaterno)
          this.usuarioService.update(this.usuario).subscribe();
          if (this.authService.hasRole('ROLE_USER')) {
            this.router.navigate(['/perfil']);
            swal.fire('Perfil Actualizado', `Felicidades ${json.cliente.nombre}: actualizaste tu perfil con éxito!`, 'success');
          } else {
            this.router.navigate(['/clientes']);
            swal.fire('Cliente Actualizado', `${json.mensaje}: ${json.cliente.nombre}`, 'success');
          }

        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      )
  }

  cargarUsuario(id2: number) {
    this.usuario.username = this.cliente.correo
    this.usuario.password = this.cliente.rut
    this.usuario.nombre = this.cliente.nombre
    this.usuario.id2 = id2
    this.usuario.apellido = this.cliente.apellidoPaterno.concat(" ", this.cliente.apellidoMaterno)
  }


}
