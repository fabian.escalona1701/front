import { Pago } from "../pago/pago";
import { ReservaEntrenamiento } from "../reserva-entrenamiento/reserva-entrenamiento";
import { ReservaEspecialista } from "../reserva-especialista/reservaEspecialista";

export class Cliente {

    id: number;
    rut: string;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    fechaNacimiento: string;
    correo: string;
    telefono: number;
    direccion: string;
    habilitado: boolean;
    foto: string;
    pagos: Array<Pago> = [];
    reservasEntrenamiento: Array<ReservaEntrenamiento> = [];
    reservasEspecialista: Array<ReservaEspecialista> = [];

}
