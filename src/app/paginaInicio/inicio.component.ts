
import { Component, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { ReservaEntrenamientoService } from '../reserva-entrenamiento/reserva-entrenamiento.service';



@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  
  
  dataBloques: any;
  dataFechas: any;
  dataAsistencia: any



  datosBloques: any[];
  datosFechas: any[];
  datosAsistencia: any[];

  arrBloques: number[] = [];
  arrFechas: number[] = [0, 0, 0, 0, 0, 0, 0];
  arrAsistencia: number[] = [0, 0];

  constructor(private reservaService: ReservaEntrenamientoService) {
    

  }


  ngOnInit(): void {
    
    this.reservaService.getCountReservasByBloque().subscribe(count => {
      this.datosBloques = count

      this.llenaArrBloques();
      this.graficoBloques();
    });

    this.reservaService.getCountReservasByFecha().subscribe(count => {
      this.datosFechas = count
      this.llenaArrFechas();
      this.graficoFechas();
    });

    this.reservaService.getCountAsistencia().subscribe(count => {
      this.datosAsistencia = count
      this.llenarArrAsistencia();
      this.graficoAsistencia();
    });

    
  }

  graficoBloques(): void {
    this.dataBloques = {
      labels: ['07:00', '08:45', '10:30', '12:15', '14:00', '15:45', '17:30', '19:15', '21:00'],
      datasets: [
        {
          label: 'Reservas por bloque',
          data: this.arrBloques,
          borderColor: '#42A5F5',
          backgroundColor: [
            "#0029D2",
            "#139128",
            "#FFA726",
            '#FF0000',
            '#FFFF00',
            '#FB00FF',
            '#00FFFF',
            '#8DFF42',
            '#000000'

          ],
          tension: .4,

        },

      ]
    }

  }

  llenaArrBloques() {
    //bloques
    let totalR = 0;
    this.datosBloques.forEach(element => {
      totalR += element[1]
    });


    for (let i = 1; i <= 9; i++) {
      this.datosBloques.forEach(element => {
        if (element[0] == i) {
          this.arrBloques.push((element[1] / totalR) * 100)
        }
      });

      if (this.arrBloques.length != i) {
        this.arrBloques.push(0);
      }
    }


  }

  graficoFechas() {
    this.dataFechas = {
      labels: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
      datasets: [
        {
          label: 'N° reservas de entrenamiento',
          data: this.arrFechas,
          borderColor: '#42A5F5',
          backgroundColor:
            "#66BB6A"
        },

      ]
    }
  }

  llenaArrFechas() {
    //fechas

    this.datosFechas.forEach(element => {
      let date = new Date(element[0] + " 12:00 pm")
      if (date.getDay() == 0) {
        this.arrFechas[6] += element[1];
      } else {
        this.arrFechas[date.getDay() - 1] += element[1];
      }
    });


  }

  graficoAsistencia() {
    this.dataAsistencia = {
      labels: ['Asiste', 'No Asiste'],
      datasets: [
        {
          label: 'Asistencia',
          data: this.arrAsistencia,
          borderColor: '#42A5F5',
          backgroundColor: [
            "#FFA726",
            "#66BB6A"
          ],
          tension: .4,

        },

      ]
    }
  }
  llenarArrAsistencia() {

    let totalR = 0;
    this.datosAsistencia.forEach(element => {
      totalR += element[1]
    });

    this.datosAsistencia.forEach(element => {
      if (element[0] == true) {
        this.arrAsistencia[0] = (element[1] / totalR) * 100;
      } else {
        this.arrAsistencia[1] = (element[1] / totalR) * 100;
      }
    });
  }

  









}