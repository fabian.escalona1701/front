import { Injectable } from '@angular/core';
import { Administrador } from './administrador';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http'; //para conectar con las peticiones http
import { map, catchError } from 'rxjs/operators';
import { AuthService } from '../usuarios/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Usuario } from '../usuarios/usuario';


@Injectable({
  providedIn: 'root'
})
export class AdministradorService {

  private urlEndPoint: string = 'http://localhost:8080/mfit/administradores'
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient, private router: Router,
    private authService: AuthService) { }


  private agregarAuthorizationHeader() {
    let token = this.authService.token;
    if (token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);

    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e): boolean {
    if (e.status == 401) {
      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }

    if (e.status == 403) {
      swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.username} pero no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/inicio']);
      return true;
    }
    return false;
  }

  getAllAdministradores(): Observable<Administrador[]> {

    return this.http.get(this.urlEndPoint, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as Administrador[])
    );
  }


  getAdministradores(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page, { headers: this.agregarAuthorizationHeader() }).pipe(
      map((response: any) => {
        (response.content as Administrador[]).map(administrador => {
          administrador.nombre = administrador.nombre.toUpperCase();
          administrador.apellidoPaterno = administrador.apellidoPaterno.toUpperCase();
          administrador.apellidoMaterno = administrador.apellidoMaterno.toUpperCase();

          return administrador;
        });
        return response;
      })
    );
  }

  create(administrador: Administrador): Observable<Administrador> {
    return this.http.post<Administrador>(this.urlEndPoint, administrador, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response.administrador as Administrador),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }

          if (e.status == 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            swal.fire(e.error.mensaje, e.error.error, 'error')
          }
          return throwError(e);
        })
      )
  }

  getAdministrador(id: any): Observable<Administrador> {
    return this.http.get<Administrador>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/inicio']);
        console.error(e.error.mensaje);
        swal.fire('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }


  update(administrador: Administrador): Observable<Administrador> {
    return this.http.put<Administrador>(`${this.urlEndPoint}/${administrador.id}`, administrador, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }

        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  delete(id: number): Observable<Administrador> {
    return this.http.delete<Administrador>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  subirFoto(archivo: File, id): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    let httpHeaders = new HttpHeaders();
    let token = this.authService.token;
    if (token != null) {
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    }

    const req = new HttpRequest('POST', `${this.urlEndPoint}/upload`, formData, {
      reportProgress: true,
      headers: httpHeaders
    });

    return this.http.request(req).pipe(
      catchError(e => {
        this.isNoAutorizado(e);
        return throwError(e);
      })
    );

  }

}

