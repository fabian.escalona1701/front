import { Component, OnInit } from '@angular/core';


import { Router, ActivatedRoute } from '@angular/router'
import swal from 'sweetalert2'
import { Usuario } from '../usuarios/usuario';
import { UsuarioService } from '../usuarios/usuario.service';
import { Administrador } from './administrador';
import { AdministradorService } from './administrador.service';

@Component({
  selector: 'app-formAdm',
  templateUrl: './formAdm.component.html'
})
export class FormAdmComponent implements OnInit {

  public administrador: Administrador = new Administrador()
  public titulo: string = "REGISTRO DE ADMINISTRADOR"
  public usuario: Usuario = new Usuario()
  public errores: string[];

  constructor(private administradorService: AdministradorService,
    private usuarioService: UsuarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarAdministrador()
  }

  public cargarAdministrador(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        this.administradorService.getAdministrador(id).subscribe((adm) => {
          this.administrador = adm
          this.usuarioService.getUsuarioByUsername(this.administrador.correo).subscribe(
            user => this.usuario = user)
        })
      }
    })
  }

  public create(): void {
    this.administradorService.create(this.administrador)
      .subscribe(
        admin => {
          this.cargarUsuario();
          console.log(admin.id)
          this.usuario.id2 = admin.id
          this.usuarioService.createUsuario(this.usuario, 4).subscribe(
            usuario => err => {
              this.errores = err.error.errors as string[];
            }
          );
          this.router.navigate(['/administradores'])
          swal.fire('Nuevo administrador', `Administrador ${this.administrador.nombre} ${this.administrador.apellidoPaterno} creado con éxito!`, 'success')
        },
        err => {
          this.errores = err.error.errors as string[];
        }
      );
  }


  public update(): void {
    this.administradorService.update(this.administrador)
      .subscribe(
        admin => {
          this.usuario.username = this.administrador.correo
          this.usuario.nombre = this.administrador.nombre
          this.usuario.apellido = this.administrador.apellidoPaterno.concat(" ", this.administrador.apellidoMaterno)
          this.usuario.password = this.administrador.rut
          this.usuarioService.update(this.usuario).subscribe();
          this.router.navigate(['/administradores'])
          swal.fire('Administrador Actualizado', `Administrador ${admin.nombre} ${admin.apellidoPaterno} actualizado con éxito!`, 'success')
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  }

  cargarUsuario() {
    this.usuario.username = this.administrador.correo
    this.usuario.password = this.administrador.rut
    this.usuario.nombre = this.administrador.nombre
    this.usuario.apellido = this.administrador.apellidoPaterno.concat(" ", this.administrador.apellidoMaterno)
  }

}