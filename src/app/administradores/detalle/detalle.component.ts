import { HttpEventType } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from 'src/app/clientes/detalle/modal.service';
import swal from 'sweetalert2';
import { Administrador } from '../administrador';
import { AdministradorService } from '../administrador.service';

@Component({
  selector: 'detalle-administrador',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleAdminComponent implements OnInit {

  @Input() administrador: Administrador;
  titulo: string = "Detalle del administrador";
  fotoSeleccionada: File;
  progreso: number = 0;

  constructor(private adminService: AdministradorService,
    public modalService: ModalService) { }

  ngOnInit(): void { }

  seleccionarFoto(event) {
    this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;

    if (this.fotoSeleccionada.type.indexOf('image') < 0) {
      swal.fire('Error seleccionando imagen: ', 'El archivo debe ser del tipo imagen', 'error')
      this.fotoSeleccionada = null;
    }
  }

  subirFoto() {

    if (!this.fotoSeleccionada) {
      swal.fire('Error Upload: ', 'Debe seleccionar una foto', 'error')
    } else {
      this.adminService.subirFoto(this.fotoSeleccionada, this.administrador.id)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body;
            this.administrador = response.administrador as Administrador;

            this.modalService.notificarUpload.emit(this.administrador);

            swal.fire('La imagen ha subido completamente', response.mensaje, 'success',)
          }

        });
    }
  }

  cerrarModal() {

    this.modalService.cerrarModal();
    this.fotoSeleccionada = null;
    this.progreso = 0;
  }

}
