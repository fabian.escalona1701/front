import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Administrador } from './administrador';
import { AdministradorService } from './administrador.service';
import swal from 'sweetalert2'
import { ModalService } from '../clientes/detalle/modal.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../usuarios/auth.service';

@Component({
  selector: 'app-administradores',
  templateUrl: './administradores.component.html',
  styleUrls: ['./administradores.component.css']
})
export class AdministradoresComponent implements OnInit {

  administradores: Administrador[] = [];

  paginador: any;

  enlacePaginador: string = 'administradores/page';

  administradorSeleccionado: Administrador;

  filterpost = '';

  constructor(private administradorService: AdministradorService,
    private modalService: ModalService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.administradorService.getAllAdministradores().subscribe(
        response => {
          this.administradores = response as Administrador[];

        });
    });
  }

  delete(administrador: Administrador): void {
    swal.fire({
      title: `Está seguro de querer eliminar a ${administrador.nombre} ${administrador.apellidoPaterno} ? `,
      text: "Esta accion no puede ser revertida",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',

    }).then((result) => {
      if (result.value) {

        this.administradorService.delete(administrador.id).subscribe(
          () => {
            this.administradores = this.administradores.filter(admin => admin !== administrador)
            swal.fire(
              'Administrador Eliminado!',
              `${administrador.nombre} ${administrador.apellidoPaterno}  eliminado con éxito.`,
              'success'
            )
          }
        )
      }
    })

    this.modalService.notificarUpload.subscribe(administrador => {
      this.administradores = this.administradores.map(adminOriginal => {
        if (administrador.id == adminOriginal.id) {
          adminOriginal.foto = administrador.foto;
        }
        return adminOriginal;
      })
    });
  }

  abrirModal(administrador: Administrador) {
    this.administradorSeleccionado = administrador;
    this.modalService.abrirModal();
  }

}
