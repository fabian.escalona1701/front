import { Cliente } from "../clientes/cliente";
import { BloqueEntrenamiento } from "./bloqueEntrenamiento";

export class ReservaEntrenamiento {
    id: number;
    fecha: string;
    asistencia: boolean;
    cliente: Cliente;
    bloque: BloqueEntrenamiento;
}