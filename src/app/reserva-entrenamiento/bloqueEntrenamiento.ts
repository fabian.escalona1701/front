import { Contador } from "./contador"
import { ReservaEntrenamiento } from "./reserva-entrenamiento"

export class BloqueEntrenamiento {
    id: number
    descripcion: string
    capacidadMaxima: number
    contador: Contador;
    reservas: Array<ReservaEntrenamiento> = [];

}