import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/usuarios/auth.service';
import { ReservaEntrenamientoService } from '../reserva-entrenamiento.service';
import swal from 'sweetalert2';
import { BloqueEntrenamiento } from '../bloqueEntrenamiento';
@Component({
  selector: 'app-cambio-aforo',
  templateUrl: './cambio-aforo.component.html',
  styleUrls: ['../../paginaInicio/inicio.component.css']
})
export class CambioAforoComponent implements OnInit {
  aforo: number = 0
  bloque: BloqueEntrenamiento = new BloqueEntrenamiento()

  constructor(
    private reservaEntrenamientoService: ReservaEntrenamientoService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public auth: AuthService,

  ) { }

  ngOnInit(): void {
    this.reservaEntrenamientoService.getBloques().subscribe(blokes => {
      this.bloque = blokes[1]
      this.aforo = this.bloque.capacidadMaxima
    })
  }

  cambiarAforo(aforo: number) {
    this.aforo = aforo;

    swal.fire({

      title: 'Confirmar aforo',
      text: 'Está seguro de modificar el aforo?',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Confirmar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.reservaEntrenamientoService.setAforo(this.aforo).subscribe()
        this.router.navigate(['/inicio']);
        swal.fire('Aforo modificado', '', 'success')
      }
    })



  }


 


}
