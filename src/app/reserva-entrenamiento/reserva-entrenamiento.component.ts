import { Component, OnInit } from '@angular/core';
import { NgbDateStruct, NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { ReservaEntrenamiento } from './reserva-entrenamiento';
import { BloqueEntrenamiento } from './bloqueEntrenamiento';
import { ActivatedRoute, Router } from '@angular/router';
import { ReservaEntrenamientoService } from './reserva-entrenamiento.service';
import swal from 'sweetalert2'
import { ClienteService } from '../clientes/cliente.service';
import { Cliente } from '../clientes/cliente';
import { Contador } from './contador';
import { AuthService } from '../usuarios/auth.service';
@Component({
  selector: 'app-reserva-entrenamiento',
  templateUrl: './reserva-entrenamiento.component.html',
  styleUrls: ['./reserva-entrenamiento.css']

})
export class ReservaEntrenamientoComponent implements OnInit {
  model: NgbDateStruct;
  date: { year: number, month: number };
  public reservaEntrenamiento: ReservaEntrenamiento = new ReservaEntrenamiento();
  public reservas: ReservaEntrenamiento[] = [];
  bloques: BloqueEntrenamiento[] = [];
  cliente: Cliente = new Cliente();
  enable: NgbDateStruct[] = [];
  fechaHoy: number;
  contadores: Contador[] = [];
  contador: number;


  constructor(private calendar: NgbCalendar,
    private reservaEntrenamientoService: ReservaEntrenamientoService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private clienteService: ClienteService,
    public auth: AuthService) { }

  ngOnInit(): void {
    this.model = this.calendar.getToday();
    this.todayandTomorrow();
    this.reservaEntrenamientoService.getContadores().subscribe(cont => this.contadores = cont);
    this.reservaEntrenamientoService.getBloques().subscribe(bloques => this.bloques = bloques);
    this.activatedRoute.paramMap.subscribe(params => {
      let clienteId = +params.get('idCliente');
      if (this.auth.isClienteAutenticated(clienteId)) {
        this.reservaEntrenamientoService.getReservasPorCliente(clienteId).subscribe(reservas => this.reservas = reservas);
        this.clienteService.getCliente(clienteId).subscribe(cliente => {
          if (!cliente.habilitado) {
            if (this.auth.hasRole('ROLE_USER')) {
              this.router.navigate(['/inicio'])
              swal.fire('Acceso Denegado', 'No posees un plan vigente para reservar', 'warning')
            } else {
              swal.fire('Acceso Denegado', `El cliente ${cliente.nombre} ${cliente.apellidoPaterno} no posee un plan vigente para reservar`, 'warning')
              this.router.navigate(['/clientes'])
            }
          }
          this.reservaEntrenamiento.cliente = cliente
        })
      }
    })

  }

  todayandTomorrow() {
    let hoy: NgbDateStruct = this.calendar.getToday();
    let tomorrow: NgbDateStruct = { day: (hoy.day + 1), month: hoy.month, year: hoy.year };
    this.enable.push(hoy, tomorrow);
  }

  public reservar(): void {
    let fechaHoy: string = this.fechaToString();
    this.reservaEntrenamiento.fecha = fechaHoy

    this.reservaEntrenamientoService.create(this.reservaEntrenamiento).subscribe(() => {
      swal.fire('Nueva Reserva de Entrenamiento', `${this.reservaEntrenamiento.cliente.nombre} ${this.reservaEntrenamiento.cliente.apellidoPaterno} reservó con éxito! `, 'success')

      if (this.auth.hasRole('ROLE_USER')) {
        this.router.navigate(['/inicio'])
      } else {
        this.router.navigate(['/clientes'])
      }


    });
  }

  public bloqueOcupado(bloqueEntrenamiento: BloqueEntrenamiento): boolean {
    if (bloqueEntrenamiento) {
      let idContador = (bloqueEntrenamiento.contador.id) - 1;
      this.contador = this.getContador(idContador);
      if (bloqueEntrenamiento.capacidadMaxima == this.getContador(idContador)) {
        return true;
      }
      return false;
    }
    return false;

  }

  fechaToString(): string {
    let dia: string = this.model.day.toString()
    if (dia.length <= 1) {
      dia = '0' + dia

    }
    let mes: string = this.model.month.toString()
    if (mes.length <= 1) {
      mes = '0' + mes

    }
    let año: string = this.model.year.toString()
    let fechaHoy: string = (año + '-' + mes + '-' + dia)
    return fechaHoy;
  }

  public yaReservo(): boolean {
    let dateSelected = this.fechaToString();
    for (let i = 0; i < this.reservas.length; i++) {
      if (this.reservas[i].fecha === dateSelected) {
        return true;
      }
    }
    return false;
  }


  getContador(bloque: number): number {
    let dateSelected = new Date(this.model.year, this.model.month - 1, this.model.day)
    let dia = dateSelected.getUTCDay();
    if (dia == 0) {
      return this.contadores[bloque].domingo;

    } else if (dia == 1) {
      return this.contadores[bloque].lunes;

    } else if (dia == 2) {
      return this.contadores[bloque].martes;

    } else if (dia == 3) {
      return this.contadores[bloque].miercoles;

    } else if (dia == 4) {
      return this.contadores[bloque].jueves;

    } else if (dia == 5) {
      return this.contadores[bloque].viernes;

    } else if (dia == 6) {
      return this.contadores[bloque].sabado;

    }
    return -1;
  }
}
