import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from 'src/app/clientes/cliente';
import { ClienteService } from 'src/app/clientes/cliente.service';
import { AuthService } from 'src/app/usuarios/auth.service';
import { ReservaEntrenamiento } from '../reserva-entrenamiento';
import { ReservaEntrenamientoService } from '../reserva-entrenamiento.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-reservas-por-cliente',
  templateUrl: './reservas-por-cliente.component.html'
})
export class ReservasEntrenamientoPorClienteComponent implements OnInit {
  reserva: ReservaEntrenamiento = new ReservaEntrenamiento;
  reservasPorCliente: ReservaEntrenamiento[] = [];
  cliente: Cliente = new Cliente;


  constructor(private reservaService: ReservaEntrenamientoService, private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,
  ) { }


  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      let clienteId = +params.get('idCliente');
      if (this.authService.isClienteAutenticated(clienteId)) {
        this.clienteService.getCliente(clienteId).subscribe(cliente => this.cliente = cliente);
        this.reservaService.getReservasPorCliente(clienteId).subscribe(reservas => this.reservasPorCliente = reservas);
      }
    })

  }

  delete(reservaEntrenamiento: ReservaEntrenamiento): void {
    let split = reservaEntrenamiento.fecha.split('-')
    let fecha = split[2].concat('/', split[1], '/', split[0])
    swal.fire({
      title: `Está seguro de querer eliminar la reserva del dia ${fecha} ? `,
      text: "Esta accion no puede ser revertida",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.reservaService.delete(reservaEntrenamiento.id).subscribe(
          response => {
            this.reservasPorCliente = this.reservasPorCliente.filter(cli => cli !== reservaEntrenamiento)
            swal.fire(
              'Reserva Eliminada!',
              `La reserva del dia ${reservaEntrenamiento.fecha}  ha sido eliminada satisfactoriamente`,
              'success'
            )
          }
        )
      }
    })
  }

  asistencia(reserva: ReservaEntrenamiento): void {
    let split = reserva.fecha.split('-')
    let fecha = split[2].concat('/', split[1], '/', split[0])

    swal.fire({

      title: `Confirmar asistencia`,
      text: `Está seguro de confirmar la asistencia del cliente ${reserva.cliente.nombre} para la fecha ${fecha}`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Confirmar',
    }).then((result) => {
      if (result.isConfirmed) {
        reserva.asistencia = true;
        this.reservaService.update(reserva).subscribe();
        this.router.navigate(['/clientes'])
        swal.fire('Asistencia confirmada', '', 'success')
      }
    })
  }

}
