import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BloqueEntrenamiento } from './bloqueEntrenamiento';
import { catchError, map } from 'rxjs/operators'
import { Router } from '@angular/router';
import { AuthService } from '../usuarios/auth.service';
import swal from 'sweetalert2';
@Injectable({
  providedIn: 'root'
})
export class BloqueEntrenamientoService {

  private urlEndPoint: string = 'http://localhost:8080/mfit/bloquesEntrenamiento'

  private httpHeaders = new HttpHeaders({ 'content-type': 'application/json' })
  constructor(private http: HttpClient, private authService: AuthService,
    private router: Router) { }

  private agregarAuthorizationHeader() {
    let token = this.authService.token;
    if (token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e): boolean {
    if (e.status == 401) {

      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }

    if (e.status == 403) {
      swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.username} pero no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/inicio']);
      return true;
    }
    return false;
  }



  public create(bloqueEntrenamiento: BloqueEntrenamiento): Observable<BloqueEntrenamiento> {
    return this.http.post<BloqueEntrenamiento>(this.urlEndPoint, bloqueEntrenamiento, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response.pago as BloqueEntrenamiento),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }

          if (e.status == 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            console.error(e.error.mensaje)
          }


          return throwError(e);
        })
      )
  }

  getbloqueEntrenamientos(): Observable<BloqueEntrenamiento[]> {

    return this.http.get(this.urlEndPoint, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as BloqueEntrenamiento[]),
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }


        return throwError(e);
      })
    )
  }
  public getbloqueEntrenamiento(id: any): Observable<BloqueEntrenamiento> {
    return this.http.get<BloqueEntrenamiento>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        this.router.navigate(['/inicio']);
        console.error(e.error.mensaje);
        swal.fire('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }

  update(bloqueEntrenamiento: BloqueEntrenamiento): Observable<BloqueEntrenamiento> {
    return this.http.put<BloqueEntrenamiento>(`${this.urlEndPoint}/${bloqueEntrenamiento.id}`, bloqueEntrenamiento, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }

        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  delete(id: number): Observable<BloqueEntrenamiento> {
    return this.http.delete<BloqueEntrenamiento>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));

  }

}