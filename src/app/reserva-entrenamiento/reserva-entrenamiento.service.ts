import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReservaEntrenamiento } from './reserva-entrenamiento';
import { BloqueEntrenamiento } from './bloqueEntrenamiento';
import { catchError, map } from 'rxjs/operators'
import { Router } from '@angular/router';
import { AuthService } from '../usuarios/auth.service';
import swal from 'sweetalert2';
import { Contador } from './contador';

@Injectable({
  providedIn: 'root'
})
export class ReservaEntrenamientoService {

  private urlEndPoint: string = 'http://localhost:8080/mfit/reserva-entrenamiento'

  private httpHeaders = new HttpHeaders({ 'content-type': 'application/json' })
  constructor(private http: HttpClient, private authService: AuthService,
    private router: Router) { }

  private agregarAuthorizationHeader() {
    let token = this.authService.token;
    if (token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e): boolean {
    if (e.status == 401) {

      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }

    if (e.status == 403) {
      swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.username} pero no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/inicio']);
      return true;
    }
    return false;
  }


  public getBloques(): Observable<BloqueEntrenamiento[]> {
    return this.http.get<BloqueEntrenamiento[]>(this.urlEndPoint + '/bloques', { headers: this.agregarAuthorizationHeader() });
  }



  public create(reservaEntrenamiento: ReservaEntrenamiento): Observable<ReservaEntrenamiento> {
    return this.http.post<ReservaEntrenamiento>(this.urlEndPoint, reservaEntrenamiento, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response as ReservaEntrenamiento),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }
          if (e.status == 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            console.error(e.error.mensaje)
          }
          return throwError(e);
        })
      )
  }

  getReservasEntrenamiento(): Observable<ReservaEntrenamiento[]> {
    return this.http.get(this.urlEndPoint, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as ReservaEntrenamiento[]),
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }


        return throwError(e);
      })
    )
  }
  public getReservaEntrenamiento(id: any): Observable<ReservaEntrenamiento> {
    return this.http.get<ReservaEntrenamiento>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        this.router.navigate(['/inicio']);
        console.error(e.error.mensaje);
        swal.fire('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }

  update(reservaEntrenamiento: ReservaEntrenamiento): Observable<ReservaEntrenamiento> {
    return this.http.put<ReservaEntrenamiento>(`${this.urlEndPoint}/${reservaEntrenamiento.id}`, reservaEntrenamiento, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  delete(id: number): Observable<ReservaEntrenamiento> {

    return this.http.delete<ReservaEntrenamiento>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  public getReservasPorCliente(id: number): Observable<ReservaEntrenamiento[]> {
    return this.http.get(`${'http://localhost:8080/mfit/clientes/reservas-entrenamiento'}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as ReservaEntrenamiento[]),
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        return throwError(e);
      })
    )

  }

  public getContadores(): Observable<Contador[]> {
    return this.http.get(`${'http://localhost:8080/mfit/contadores'}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as Contador[]),
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        return throwError(e);
      })
    )
  }

  public getCountReservasByBloque(): Observable<any[]> {
    return this.http.get<any[]>(this.urlEndPoint + '/countByBloque', { headers: this.agregarAuthorizationHeader() });

  }

  public getCountReservasByFecha(): Observable<any[]> {
    return this.http.get<any[]>(this.urlEndPoint + '/countByFecha', { headers: this.agregarAuthorizationHeader() });

  }

  public getCountAsistencia(): Observable<any[]> {
    return this.http.get<any[]>(this.urlEndPoint + '/countAsistencia', { headers: this.agregarAuthorizationHeader() });

  }

  public setAforo(aforo: number): Observable<any> {
    console.log("setAforo")
    return this.http.put<any>(`${'http://localhost:8080/mfit/cambio-aforo'}`, aforo, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }



}

