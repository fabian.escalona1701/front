import { Component, OnInit } from '@angular/core';
import { NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Cliente } from '../clientes/cliente';
import { ReservaEspecialista } from './reservaEspecialista';
import { Especialista } from '../especialistas/especialista';
import { ActivatedRoute, Router } from '@angular/router';
import { ClienteService } from '../clientes/cliente.service';
import { ReservaEspecialistaService } from './reserva-especialista.service';
import { EspecialistaService } from '../especialistas/especialista.service';
import swal from 'sweetalert2'
import { AuthService } from '../usuarios/auth.service';

@Component({
  selector: 'app-reserva-especialista',
  templateUrl: './reserva-especialista.component.html',
  styleUrls: ['./reserva-especialista.component.css']
})
export class ReservaEspecialistaComponent implements OnInit {
  model: NgbDateStruct;
  date: { year: number, month: number };
  public reservaEspecialista: ReservaEspecialista = new ReservaEspecialista();
  reservas: ReservaEspecialista[] = [];
  reservasPorCliente: ReservaEspecialista[] = []
  cliente: Cliente;
  especialista: Especialista;
  especialistas: Especialista[] = [];
  ocupados: number[] = [];
  enable: NgbDateStruct[] = [];

  horarios: string[] = ["10:00 - 10:30", "10:30 - 11:00", "11:00 - 11:30", "11:30 - 12:00", "12:00 - 12:30",
    "12:30 - 13:00", "15:00 - 15:30", "15:30 - 16:00", "16:00 - 16:30", "16:30 - 17:00", "17:00 - 17:30",
    "17:30 - 18:00", "18:00 - 18:30", "18:30 - 19:00", "19:00 - 19:30", "19:30 - 20:00"]



  constructor(private calendar: NgbCalendar,
    private reservaEspecialistaService: ReservaEspecialistaService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private clienteService: ClienteService,
    private especialistaService: EspecialistaService,
    public auth: AuthService) { }

  ngOnInit(): void {
    this.model = this.calendar.getToday();
    this.todayandTomorrow();
    this.activatedRoute.paramMap.subscribe(params => {
      let clienteId = +params.get('idCliente');
      if (this.auth.isClienteAutenticated(clienteId)) {
        this.clienteService.getCliente(clienteId).subscribe(cliente => {
          if (!cliente.habilitado) {
            if (this.auth.hasRole('ROLE_USER')) {
              this.router.navigate(['/inicio'])
              swal.fire('Acceso Denegado', 'No posees un plan vigente para reservar', 'warning')
            } else {
              swal.fire('Acceso Denegado', `El cliente ${cliente.nombre} ${cliente.apellidoPaterno} no posee un plan vigente para reservar`, 'warning')
              this.router.navigate(['/clientes'])
            }
          }
          this.reservaEspecialista.cliente = cliente
          this.especialistaService.getAllEspecialistas().subscribe(especialistas => this.especialistas = especialistas);
          this.reservaEspecialistaService.getReservasEspecialistas().subscribe(reservas => this.reservas = reservas);
          this.reservaEspecialistaService.getReservasPorCliente(clienteId).subscribe(reservas => this.reservasPorCliente = reservas)
        })
      }
    })

  }

  todayandTomorrow() {
    let hoy: NgbDateStruct = this.calendar.getToday();
    let tomorrow: NgbDateStruct = { day: (hoy.day + 1), month: hoy.month, year: hoy.year };
    this.enable.push(hoy, tomorrow);
  }

  public reservar(): void {
    let fechaHoy = this.calculaFecha();
    this.reservaEspecialista.fecha = fechaHoy

    this.reservaEspecialistaService.create(this.reservaEspecialista).subscribe(() => {
      swal.fire('Nueva Reserva de Especialista', `${this.reservaEspecialista.cliente.nombre} ${this.reservaEspecialista.cliente.apellidoPaterno} reservó con éxito! `, 'success')
      if (this.auth.hasRole('ROLE_USER')) {
        this.router.navigate(['/inicio'])
      } else {
        this.router.navigate(['/clientes'])
      }
    });
  }

  calculaFecha(): string {
    let dia: string = this.model.day.toString()
    if (dia.length <= 1) {
      dia = '0' + dia
    }
    let mes: string = this.model.month.toString()
    if (mes.length <= 1) {
      mes = '0' + mes

    }

    let año: string = this.model.year.toString()
    let fechaHoy: string = (año + '-' + mes + '-' + dia)
    return fechaHoy
  }

  disponibilidad(especialista: Especialista): void {
    if (especialista) {
      this.ocupados = [];
      let fecha = this.calculaFecha();
      for (let reserva of this.reservas) {
        if (reserva.especialista.id == especialista.id && reserva.fecha == fecha) {
          this.ocupados.push(reserva.bloque);
        }
      }
    }
  };

  public includes(bloque: number): boolean {
    if (this.ocupados.length > 0) {
      if (this.ocupados.includes(bloque)) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }

  public yaReservo(especialista: Especialista): boolean {
    let dateSelected = this.calculaFecha();
    for (let i = 0; i < this.reservasPorCliente.length; i++) {
      if (especialista) {
        if (this.reservasPorCliente[i].fecha === dateSelected) {
          if (this.reservasPorCliente[i].especialista.id == especialista.id) {
            return true;
          }
        }
      }
    }
    return false;
  }




}

