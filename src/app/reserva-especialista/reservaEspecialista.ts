import { Cliente } from "../clientes/cliente";
import { Especialista } from '../especialistas/especialista';

export class ReservaEspecialista {
    id: number;
    fecha: string;
    asistencia: boolean
    cliente: Cliente;
    especialista: Especialista;
    bloque: number;
    horario: string;
}