import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from 'src/app/clientes/cliente';
import { ClienteService } from 'src/app/clientes/cliente.service';
import { ReservaEspecialistaService } from '../reserva-especialista.service';
import { ReservaEspecialista } from '../reservaEspecialista';
import swal from 'sweetalert2';
import { AuthService } from 'src/app/usuarios/auth.service';
@Component({
  selector: 'app-reservas-por-cliente',
  templateUrl: './reservas-por-cliente.component.html'
})
export class ReservasEspecialistaPorClienteComponent implements OnInit {
  reserva: ReservaEspecialista = new ReservaEspecialista;
  reservasPorCliente: ReservaEspecialista[] = [];
  cliente: Cliente = new Cliente;

  horarios: string[] = ["10:00 - 10:30", "10:30 - 11:00", "11:00 - 11:30", "11:30 - 12:00", "12:00 - 12:30",
    "12:30 - 13:00", "15:00 - 15:30", "15:30 - 16:00", "16:00 - 16:30", "16:30 - 17:00", "17:00 - 17:30",
    "17:30 - 18:00", "18:00 - 18:30", "18:30 - 19:00", "19:00 - 19:30", "19:30 - 20:00"]

  constructor(private reservaService: ReservaEspecialistaService, private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,
  ) { }


  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      let clienteId = +params.get('idCliente');
      if (this.authService.isClienteAutenticated(clienteId)) {
        this.clienteService.getCliente(clienteId).subscribe(cliente => this.cliente = cliente);
        this.reservaService.getReservasPorCliente(clienteId).subscribe(reservas => this.reservasPorCliente = reservas);
      }

    })

  }

  delete(ReservaEspecialista: ReservaEspecialista): void {
    swal.fire({
      title: `Está seguro de querer eliminar la reserva del dia ${ReservaEspecialista.fecha} ? `,
      text: "Esta accion no puede ser revertida",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.reservaService.delete(ReservaEspecialista.id).subscribe(
          response => {
            this.reservasPorCliente = this.reservasPorCliente.filter(cli => cli !== ReservaEspecialista)
            swal.fire(
              'Reserva Eliminada !',
              `La reserva del dia ${ReservaEspecialista.fecha}  ha sido eliminada satisfactoriamente`,
              'success'
            )
          }
        )
      }
    })
  }

  asistencia(reserva: ReservaEspecialista): void {
    let split = reserva.fecha.split('-')
    let fecha = split[2].concat('/', split[1], '/', split[0])

    swal.fire({

      title: `Confirmar asistencia`,
      text: `Está seguro de confirmar la asistencia del cliente ${reserva.cliente.nombre} para la fecha ${fecha}`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Confirmar',
    }).then((result) => {
      if (result.isConfirmed) {
        reserva.asistencia = true;
        this.reservaService.update(reserva).subscribe();
        this.router.navigate(['/clientes'])
        swal.fire('Asistencia confirmada', '', 'success')
      }
    })
  }

}
