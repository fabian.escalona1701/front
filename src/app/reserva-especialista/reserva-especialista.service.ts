import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { AuthService } from "../usuarios/auth.service";
import { ReservaEspecialista } from "./reservaEspecialista";
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class ReservaEspecialistaService {

  private urlEndPoint: string = 'http://localhost:8080/mfit/reserva-especialista'

  private httpHeaders = new HttpHeaders({ 'content-type': 'application/json' })

  constructor(private http: HttpClient, private authService: AuthService,
    private router: Router) { }

  private agregarAuthorizationHeader() {
    let token = this.authService.token;
    if (token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e): boolean {
    if (e.status == 401) {

      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }

    if (e.status == 403) {
      swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.username} pero no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/inicio']);
      return true;
    }
    return false;
  }





  public create(reservaEspecialista: ReservaEspecialista): Observable<ReservaEspecialista> {
    return this.http.post<ReservaEspecialista>(this.urlEndPoint, reservaEspecialista, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response as ReservaEspecialista),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }

          if (e.status == 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            console.error(e.error.mensaje)
          }


          return throwError(e);
        })
      )
  }

  getReservasEspecialistas(): Observable<ReservaEspecialista[]> {

    return this.http.get(this.urlEndPoint, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as ReservaEspecialista[]),
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }


        return throwError(e);
      })
    )
  }
  public getReservaEspecialista(id: any): Observable<ReservaEspecialista> {
    return this.http.get<ReservaEspecialista>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        this.router.navigate(['/inicio']);
        console.error(e.error.mensaje);
        swal.fire('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }



  update(reservaEspecialista: ReservaEspecialista): Observable<ReservaEspecialista> {
    return this.http.put<ReservaEspecialista>(`${this.urlEndPoint}/${reservaEspecialista.id}`, reservaEspecialista, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  delete(id: number): Observable<ReservaEspecialista> {
    return this.http.delete<ReservaEspecialista>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }


  public getReservasPorCliente(id: number): Observable<ReservaEspecialista[]> {

    return this.http.get(`${'http://localhost:8080/mfit/clientes/reservas-especialista'}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as ReservaEspecialista[]),
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        return throwError(e);
      })
    )


  }

  public getReservasPorEspecialista(id: number): Observable<ReservaEspecialista[]> {
    return this.http.get(`${'http://localhost:8080/mfit/especialistas/reservas-diarias'}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as ReservaEspecialista[]),
      catchError(e => {
        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        return throwError(e);
      })
    )



  }

}

