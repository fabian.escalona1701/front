/*este componente se usa para meter la barra de navegacion completa y separarlo del codigo html principal,asi queda mas ordenado*/
import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AuthService } from '../usuarios/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Cliente } from '../clientes/cliente';
import { ClienteService } from '../clientes/cliente.service';
import { Especialista } from '../especialistas/especialista';
import { EspecialistaService } from '../especialistas/especialista.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['header.css']

})
export class HeaderComponent {




    constructor(public auth: AuthService,
        private especialistaService: EspecialistaService,
        private router: Router) { }

    logout(): void {
        swal.fire('Logout', `Adiós ${this.auth.usuario.username}, has cerrado sesión con éxito`, 'success');
        this.auth.logout();
        this.router.navigate(['/inicio']);

        this.auth.usuario.id2
    }





}