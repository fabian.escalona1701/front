import { NgModule, Component, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './paginaInicio/inicio.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgChartsModule } from 'ng2-charts';

import { FilterPipe } from './clientes/busquedaCli/busqueda-cli.pipe.ts.pipe.spec';


import { MatSliderModule } from '@angular/material/slider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

//primeng
import { ButtonModule } from 'primeng/button';
import { GMapModule } from 'primeng/gmap';
import { ChartModule } from 'primeng/chart';
import { ScrollTopModule } from 'primeng/scrolltop';
import { ScrollPanelModule } from "primeng/scrollpanel";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//Administradores
import { AdministradoresComponent } from './administradores/administradores.component';
import { AdministradorService } from './administradores/administrador.service';
import { FormAdmComponent } from './administradores/formAdm.component';
//clientes
import { ClienteService } from './clientes/cliente.service';
import { FormComponent } from './clientes/form.component';

import { HeaderComponent } from './header/header.component';




//Especialistas
import { EspecialistasComponent } from './especialistas/especialistas.component';
import { FormEspComponent } from './especialistas/formEsp.component';
import { EspecialistaService } from './especialistas/especialista.service';

//Recepcionistas
import { RecepcionistasComponent } from './recepcionistas/recepcionistas.component';
import { FormRecComponent } from './recepcionistas/formRec.component';
import { RecepcionistaService } from './recepcionistas/recepcionista.service';

//pago
import { PagoComponent } from './pago/pago.component';
import { pagoFormComponent } from './pago/pagoForm.component';
import { PagoService } from './pago/pago.service';
import { PagosPorClienteComponent } from './pago/pagosPorCliente.component';



import { ReservaEntrenamientoComponent } from './reserva-entrenamiento/reserva-entrenamiento.component';
import { ReservasEntrenamientoPorClienteComponent } from './reserva-entrenamiento/reservas-por-cliente/reservas-por-cliente.component';

import { BusquedaClienteComponent } from './clientes/busquedaCli/busquedaCliente.component';
import { DetalleComponent } from './clientes/detalle/detalle.component';
import { sliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './usuarios/login.component';
import { AuthGuard } from './usuarios/guards/auth.guard';
import { RoleGuard } from './usuarios/guards/role.guard';
import { DetalleAdminComponent } from './administradores/detalle/detalle.component';
import { DetalleEspecialistaComponent } from './especialistas/detalle/detalle.component';
import { DetalleRecepcionistaComponent } from './recepcionistas/detalle/detalle.component';


//ReservaEspecialista
import { ReservaEspecialistaComponent } from './reserva-especialista/reserva-especialista.component';
import { ReservaEspecialistaService } from './reserva-especialista/reserva-especialista.service';
import { ReservasEspecialistaPorClienteComponent } from './reserva-especialista/reservas-por-cliente/reservas-por-cliente.component';
import { PerfilComponent } from './clientes/perfil.component';
import { CambioPasswordComponent } from './clientes/cambio-password/cambio-password.component';
import { ReservasDiariasComponent } from './especialistas/reservas-diarias/reservas-diarias.component';
import { CambioAforoComponent } from './reserva-entrenamiento/cambio-aforo/cambio-aforo.component';


const routes: Routes = [
  { path: '', redirectTo: '/inicio', pathMatch: 'full' },
  { path: 'inicio', component: InicioComponent },
  { path: 'busquedaClientes', component: BusquedaClienteComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'clientes', component: BusquedaClienteComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'clientes/form', component: FormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'clientes/form/:id', component: FormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN', 'ROLE_USER'] } },

  { path: 'pagos/:idCliente', component: pagoFormComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'clientes/pagos/:idCliente', component: PagosPorClienteComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN', 'ROLE_USER'] } },

  { path: 'pagos', component: PagoComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },

  { path: 'perfil', component: PerfilComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER'] } },


  { path: 'administradores', component: AdministradoresComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'administradores/page/:page', component: AdministradoresComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'administradores/form', component: FormAdmComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'administradores/form/:id', component: FormAdmComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },

  { path: 'especialistas', component: EspecialistasComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'especialistas/form', component: FormEspComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'especialistas/form/:id', component: FormEspComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },

  { path: 'recepcionistas', component: RecepcionistasComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'recepcionistas/form', component: FormRecComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },
  { path: 'recepcionistas/form/:id', component: FormRecComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },

  { path: 'reserva-especialista/:idCliente', component: ReservaEspecialistaComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER', 'ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },

  { path: 'reserva-entrenamiento/:idCliente', component: ReservaEntrenamientoComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER', 'ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },
  { path: 'login', component: LoginComponent },

  { path: 'clientes/reservas-entrenamiento/:idCliente', component: ReservasEntrenamientoPorClienteComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER', 'ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },

  { path: 'clientes/reservas-especialista/:idCliente', component: ReservasEspecialistaPorClienteComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER', 'ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },

  { path: 'clientes/password/:idCliente', component: CambioPasswordComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_USER', 'ROLE_RECEPCIONISTA', 'ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },

  { path: 'especialistas/reservas-diarias/:idEspecialista', component: ReservasDiariasComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ESPECIALISTA', 'ROLE_ADMIN'] } },

  { path: 'cambio-aforo', component: CambioAforoComponent, canActivate: [AuthGuard, RoleGuard], data: { role: ['ROLE_ADMIN'] } },






]
@NgModule({
  declarations: [
    AppComponent,
    AdministradoresComponent,
    HeaderComponent,
    InicioComponent,
    FormComponent,
    EspecialistasComponent,
    FormEspComponent,
    RecepcionistasComponent,
    FormRecComponent,
    PagoComponent,
    pagoFormComponent,
    FormAdmComponent,
    ReservaEntrenamientoComponent,
    ReservaEspecialistaComponent,
    PagosPorClienteComponent,
    BusquedaClienteComponent,
    DetalleComponent,
    sliderComponent,
    FooterComponent,
    LoginComponent,
    DetalleAdminComponent,
    DetalleEspecialistaComponent,
    DetalleRecepcionistaComponent,
    ReservasEntrenamientoPorClienteComponent,
    ReservasEspecialistaPorClienteComponent,
    FilterPipe,
    PerfilComponent,
    CambioPasswordComponent,
    ReservasDiariasComponent,
    CambioAforoComponent


  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule,
    NgbModule,
    ButtonModule,
    GMapModule,
    ChartModule,
    ScrollTopModule,
    ScrollPanelModule,
    MatSliderModule,
    MatToolbarModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatInputModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    NgChartsModule
  ],
  providers: [ClienteService, AdministradorService, EspecialistaService, RecepcionistaService, PagoService, ReservaEspecialistaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
