import { HttpEventType } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ModalService } from 'src/app/clientes/detalle/modal.service';
import swal from 'sweetalert2';
import { Especialista } from '../especialista';
import { EspecialistaService } from '../especialista.service';

@Component({
  selector: 'detalle-especialista',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleEspecialistaComponent implements OnInit {

  @Input() especialista: Especialista;
  titulo: string = "Detalle del especialista";
  fotoSeleccionada: File;
  progreso: number = 0;

  constructor(private especialistaService: EspecialistaService,
    public modalService: ModalService) { }

  ngOnInit(): void { }

  seleccionarFoto(event) {
    this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;

    if (this.fotoSeleccionada.type.indexOf('image') < 0) {
      swal.fire('Error seleccionando imagen: ', 'El archivo debe ser del tipo imagen', 'error')
      this.fotoSeleccionada = null;
    }
  }

  subirFoto() {

    if (!this.fotoSeleccionada) {
      swal.fire('Error Upload: ', 'Debe seleccionar una foto', 'error')
    } else {
      this.especialistaService.subirFoto(this.fotoSeleccionada, this.especialista.id)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body;
            this.especialista = response.especialista as Especialista;

            this.modalService.notificarUpload.emit(this.especialista);

            swal.fire('La imagen ha subido completamente', response.mensaje, 'success',)
          }

        });
    }
  }

  cerrarModal() {

    this.modalService.cerrarModal();
    this.fotoSeleccionada = null;
    this.progreso = 0;
  }

}