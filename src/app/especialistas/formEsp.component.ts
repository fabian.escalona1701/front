import { Component, OnInit } from '@angular/core';
import { Especialista } from './especialista';
import { EspecialistaService } from './especialista.service'
import { Router, ActivatedRoute } from '@angular/router'
import swal from 'sweetalert2'
import { Usuario } from '../usuarios/usuario';
import { UsuarioService } from '../usuarios/usuario.service';

@Component({
  selector: 'app-formEsp',
  templateUrl: './formEsp.component.html'
})
export class FormEspComponent implements OnInit {

  public especialista: Especialista = new Especialista()
  public usuario: Usuario = new Usuario()
  public titulo: string = "REGISTRO DE ESPECIALISTA"
  public errores: string[];

  constructor(private especialistaService: EspecialistaService,
    private usuarioService: UsuarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.cargarEspecialista()
  }

  public cargarEspecialista(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      if (id) {
        this.especialistaService.getEspecialista(id).subscribe((especialista) => {
          this.especialista = especialista
          this.usuarioService.getUsuarioByUsername(this.especialista.correo).subscribe(
            user => this.usuario = user)
        })
      }
    })
  }

  public create(): void {

    this.especialistaService.create(this.especialista)
      .subscribe(
        especialista => {
          this.cargarUsuario();
          this.usuario.id2 = especialista.id
          this.usuarioService.createUsuario(this.usuario, 2).subscribe(
            usuario => err => {
              this.errores = err.error.errors as string[];
            }
          );
          this.router.navigate(['/especialistas'])
          swal.fire('Nuevo especialista', `Especialista ${especialista.nombre}  creado con éxito!`, 'success')
        },
        err => {
          this.errores = err.error.errors as string[];
        }
      );
  }

  update(): void {

    this.especialistaService.update(this.especialista)
      .subscribe(
        json => {
          this.usuario.username = this.especialista.correo
          this.usuario.nombre = this.especialista.nombre
          this.usuario.apellido = this.especialista.apellidoPaterno.concat(" ", this.especialista.apellidoMaterno)
          this.usuario.password = this.especialista.rut
          this.usuarioService.update(this.usuario).subscribe();
          this.router.navigate(['/especialistas']);
          swal.fire('Especialista Actualizado', `${json.mensaje}: ${json.especialista.nombre}`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      )
  }

  cargarUsuario() {
    this.usuario.username = this.especialista.correo
    this.usuario.password = this.especialista.rut
    this.usuario.nombre = this.especialista.nombre
    this.usuario.apellido = this.especialista.apellidoPaterno.concat(" ", this.especialista.apellidoMaterno)
  }


}