import { Injectable } from '@angular/core';
import { Especialista } from './especialista';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators'
import { AuthService } from '../usuarios/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { Usuario } from '../usuarios/usuario';

@Injectable({
  providedIn: 'root'
})
export class EspecialistaService {

  private urlEndPoint: string = 'http://localhost:8080/mfit/especialistas'

  private httpHeaders = new HttpHeaders({ 'content-type': 'application/json' })
  constructor(private http: HttpClient, private router: Router,
    private authService: AuthService) { }

  private agregarAuthorizationHeader() {
    let token = this.authService.token;
    if (token != null) {
      return this.httpHeaders.append('Authorization', 'Bearer ' + token);
    }
    return this.httpHeaders;
  }

  private isNoAutorizado(e): boolean {
    if (e.status == 401) {

      if (this.authService.isAuthenticated()) {
        this.authService.logout();
      }
      this.router.navigate(['/login']);
      return true;
    }

    if (e.status == 403) {
      swal.fire('Acceso denegado', `Lo sentimos ${this.authService.usuario.username} pero no tienes acceso a este recurso`, 'warning');
      this.router.navigate(['/inicio']);
      return true;
    }
    return false;
  }


  create(especialista: Especialista): Observable<Especialista> {
    return this.http.post<Especialista>(this.urlEndPoint, especialista, { headers: this.agregarAuthorizationHeader() })
      .pipe(
        map((response: any) => response.especialista as Especialista),
        catchError(e => {
          if (this.isNoAutorizado(e)) {
            return throwError(e);
          }

          if (e.status == 400) {
            return throwError(e);
          }
          if (e.error.mensaje) {
            swal.fire(e.error.mensaje, e.error.error, 'error')
          }


          return throwError(e);
        })
      )
  }

  getAllEspecialistas(): Observable<Especialista[]> {

    return this.http.get(this.urlEndPoint, { headers: this.agregarAuthorizationHeader() }).pipe(
      map(response => response as Especialista[])
    );
  }

  getEspecialistas(page: number): Observable<any> {

    return this.http.get(this.urlEndPoint + '/page/' + page, { headers: this.agregarAuthorizationHeader() }).pipe(
      map((response: any) => {
        (response.content as Especialista[]).map(especialista => {
          especialista.nombre = especialista.nombre.toUpperCase();
          especialista.apellidoPaterno = especialista.apellidoPaterno.toUpperCase();
          especialista.apellidoMaterno = especialista.apellidoMaterno.toUpperCase();

          return especialista;
        });
        return response;
      })
    );
  }

  getEspecialista(id: any): Observable<Especialista> {
    return this.http.get<Especialista>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }

        this.router.navigate(['/inicio']);
        console.error(e.error.mensaje);
        swal.fire('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }


  update(especialista: Especialista): Observable<any> {
    return this.http.put<Especialista>(`${this.urlEndPoint}/${especialista.id}`, especialista, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        if (e.status == 400) {
          return throwError(e);
        }

        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  delete(id: number): Observable<Especialista> {
    return this.http.delete<Especialista>(`${this.urlEndPoint}/${id}`, { headers: this.agregarAuthorizationHeader() }).pipe(
      catchError(e => {

        if (this.isNoAutorizado(e)) {
          return throwError(e);
        }
        console.error(e.error.mensaje);
        swal.fire(e.error.mensaje, e.error.error, 'error');
        return throwError(e);
      }));
  }

  subirFoto(archivo: File, id): Observable<HttpEvent<{}>> {
    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    let httpHeaders = new HttpHeaders();
    let token = this.authService.token;
    if (token != null) {
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    }

    const req = new HttpRequest('POST', `${this.urlEndPoint}/upload`, formData, {
      reportProgress: true,
      headers: httpHeaders
    });

    return this.http.request(req).pipe(
      catchError(e => {
        this.isNoAutorizado(e);
        return throwError(e);
      })
    );

  }

}

