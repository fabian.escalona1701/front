import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { Especialista } from './especialista';
import { EspecialistaService } from './especialista.service'
import swal from 'sweetalert2'
import { ModalService } from '../clientes/detalle/modal.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../usuarios/auth.service';

@Component({
  selector: 'app-especialistas',
  templateUrl: './especialistas.component.html',
  styleUrls: ['./especialistas.component.css']
})
export class EspecialistasComponent implements OnInit {

  especialistas: Especialista[] = [];

  paginador: any;

  especialistaSeleccionado: Especialista;

  filterpost = '';

  enlacePaginador: string = 'especialistas/page';

  constructor(private especialistaService: EspecialistaService,
    private modalService: ModalService,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService) { }

  ngOnInit(): void {


    this.activatedRoute.paramMap.subscribe(params => {
      this.especialistaService.getAllEspecialistas().subscribe(
        response => {
          this.especialistas = response as Especialista[];

        });
    });

    this.modalService.notificarUpload.subscribe(especialista => {
      this.especialistas = this.especialistas.map(especialistaOriginal => {
        if (especialista.id == especialistaOriginal.id) {
          especialistaOriginal.foto = especialista.foto;
        }
        return especialistaOriginal;
      })
    })
  }



  delete(especialista: Especialista): void {
    swal.fire({
      title: `Está seguro de querer eliminar a ${especialista.nombre} ${especialista.apellidoPaterno} ? `,
      text: "Esta accion no puede ser revertida",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.especialistaService.delete(especialista.id).subscribe(
          response => {
            this.especialistas = this.especialistas.filter(esp => esp !== especialista)
            swal.fire(
              'Especialista Eliminado!',
              `El especialista ${especialista.nombre} ${especialista.apellidoPaterno} ha sido eliminado satisfactoriamente`,
              'success'
            )
          }
        )
      }
    })
  }

  abrirModal(especialista: Especialista) {
    this.especialistaSeleccionado = especialista;
    this.modalService.abrirModal();
  }

}



