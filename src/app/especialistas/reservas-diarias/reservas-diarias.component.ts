import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReservaEspecialistaService } from 'src/app/reserva-especialista/reserva-especialista.service';
import { AuthService } from 'src/app/usuarios/auth.service';
import { ReservaEspecialista } from '../../reserva-especialista/reservaEspecialista';
import { Especialista } from '../especialista';
import { EspecialistaService } from '../especialista.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-reservas-diarias',
  templateUrl: './reservas-diarias.component.html'
})
export class ReservasDiariasComponent implements OnInit {


  reserva: ReservaEspecialista = new ReservaEspecialista;
  reservasPorEspecialista: ReservaEspecialista[] = [];
  especialista: Especialista = new Especialista;


  constructor(private reservaService: ReservaEspecialistaService, private especialistaService: EspecialistaService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public authService: AuthService,) { }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe(params => {
      let especialistaId = +params.get('idEspecialista');
      if (this.authService.isClienteAutenticated(especialistaId)) {
        this.especialistaService.getEspecialista(especialistaId).subscribe(especialista => this.especialista = especialista);
        this.reservaService.getReservasPorEspecialista(especialistaId).subscribe(reservas => this.reservasPorEspecialista = reservas);
      }
    })
  }

  delete(reservaEspecialista: ReservaEspecialista): void {
    let split = reservaEspecialista.fecha.split('-')
    let fecha = split[2].concat('/', split[1], '/', split[0])
    swal.fire({
      title: `Está seguro de querer eliminar la reserva del dia ${fecha} ? `,
      text: "Esta accion no puede ser revertida",
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.reservaService.delete(reservaEspecialista.id).subscribe(
          response => {
            this.reservasPorEspecialista = this.reservasPorEspecialista.filter(cli => cli !== reservaEspecialista)
            swal.fire(
              'Reserva Eliminada!',
              `La reserva del dia ${reservaEspecialista.fecha}  ha sido eliminada satisfactoriamente`,
              'success'
            )
          }
        )
      }
    })
  }

  asistencia(reserva: ReservaEspecialista): void {
    let split = reserva.fecha.split('-')
    let fecha = split[2].concat('/', split[1], '/', split[0])

    swal.fire({

      title: `Confirmar asistencia`,
      text: `Está seguro de confirmar la asistencia del cliente ${reserva.cliente.nombre} para la fecha ${fecha}`,
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Confirmar',
    }).then((result) => {
      if (result.isConfirmed) {
        reserva.asistencia = true;
        this.reservaService.update(reserva).subscribe();
        //this.router.navigate(['/especialistas/reservas-diarias/:idEspecialista'])
        swal.fire('Asistencia confirmada', '', 'success')
      }
    })
  }


  calcularHorario(bloque: number, reserva: ReservaEspecialista) {
    switch (bloque) {
      case 1: {
        reserva.horario = '10:00-10:30'
        break;
      }
      case 2: {
        reserva.horario = '10:30-11:00'
        break;
      }
      case 3: {
        reserva.horario = '11:00-11:30'
        break;
      }
      case 4: {
        reserva.horario = '11:30-12:00'
        break;
      }
      case 5: {
        reserva.horario = '12:00-12:30'
        break;
      }
      case 6: {
        reserva.horario = '12:30-13:00'
        break;
      }
      case 7: {
        reserva.horario = '15:00-15:30'
        break;
      }
      case 8: {
        reserva.horario = '15:30-16:00'
        break;
      }
      case 9: {
        reserva.horario = '16:00-16:30'
        break;
      }
      case 10: {
        reserva.horario = '16:30-17:00'
        break;
      }
      case 11: {
        reserva.horario = '17:00-17:30'
        break;
      }
      case 12: {
        reserva.horario = '17:30-18:00'
        break;
      }
      case 13: {
        reserva.horario = '18:00-18:30'
        break;
      }
      case 14: {
        reserva.horario = '18:30-19:00'
        break;
      }
      case 15: {
        reserva.horario = '19:00-19:30'
        break;
      }
      case 16: {
        reserva.horario = '19:30-20:00'
        break;
      }


    }
    console.log("hola")
    if (bloque = 1) {
      console.log("dentro del if")

    }

  }


}
