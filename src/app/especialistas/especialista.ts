import { ReservaEspecialista } from '../reserva-especialista/reservaEspecialista';
export class Especialista {
    id: number;
    rut: string;
    nombre: string;
    apellidoPaterno: string;
    apellidoMaterno: string;
    fechaNacimiento: string;
    correo: string;
    telefono: number;
    direccion: string;
    sueldo: number;
    especialidad: number;
    foto: string;
    reservas: Array<ReservaEspecialista> = [];



}
